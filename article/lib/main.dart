import 'package:flutter/material.dart';
import 'home.dart';
import 'login.dart';


void main() async {
  Widget _defaultHome = LoginScreen1();


  

  final routes = <String, WidgetBuilder>{
   
    Home.tag :(context)=>Home(),
    LoginScreen1.tag :(conext) =>LoginScreen1()
  };
  // Run app!
  runApp(MaterialApp(
    title: 'UnFound',
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      primarySwatch: Colors.lightBlue,
      fontFamily: 'Nunito',
    ),
    home: _defaultHome,
    routes: routes,
  ));
}
