import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_offline/flutter_offline.dart';

Future<Map> getStatusTokenList() async {
  var output;
  final response = await http.get(
      'https://newsapi.org/v2/everything?q=apple&from=2019-05-08&to=2019-05-08&sortBy=popularity&apiKey=b7569dc49e3f4b9284cd6a63f6f92be2');

  if (response != null && response.statusCode == 200) {
    var data = jsonDecode(response.body);
    print(data);
    output = data;

// s
  } else {
    var data = jsonDecode(response.body);
    print(data);
  }

  return output;
}

Future<Map> getIndiaList() async {
  var output;
  final response = await http.get(
      'https://newsapi.org/v2/everything?q=india&from=2019-05-08&to=2019-05-08&sortBy=popularity&apiKey=b7569dc49e3f4b9284cd6a63f6f92be2');

  if (response != null && response.statusCode == 200) {
    var data = jsonDecode(response.body);
    print(data);
    output = data;

// s
  } else {
    var data = jsonDecode(response.body);
    print(data);
  }

  return output;
}

Future<Map> getWorldList() async {
  var output;
  final response = await http.get(
      'https://newsapi.org/v2/everything?q=world&from=2019-05-08&to=2019-05-08&sortBy=popularity&apiKey=b7569dc49e3f4b9284cd6a63f6f92be2');

  if (response != null && response.statusCode == 200) {
    var data = jsonDecode(response.body);
    print(data);
    output = data;

// s
  } else {
    var data = jsonDecode(response.body);
    print(data);
  }

  return output;
}

Future<Map> getBusinessList() async {
  var output;
  final response = await http.get(
      'https://newsapi.org/v2/everything?q=business&from=2019-05-08&to=2019-05-08&sortBy=popularity&apiKey=b7569dc49e3f4b9284cd6a63f6f92be2');

  if (response != null && response.statusCode == 200) {
    var data = jsonDecode(response.body);
    print(data);
    output = data;

// s
  } else {
    var data = jsonDecode(response.body);
    print(data);
  }

  return output;
}

Future<Map> getTechList() async {
  var output;
  final response = await http.get(
      'https://newsapi.org/v2/everything?q=technology&from=2019-05-08&to=2019-05-08&sortBy=popularity&apiKey=b7569dc49e3f4b9284cd6a63f6f92be2');

  if (response != null && response.statusCode == 200) {
    var data = jsonDecode(response.body);
    print(data);
    output = data;

// s
  } else {
    var data = jsonDecode(response.body);
    print(data);
  }

  return output;
}

class Home extends StatefulWidget {
  static String tag = 'home';
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var k = getStatusTokenList();
  bool bookmark = true;
  List book = [];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        backgroundColor: Color(0xFFf7f7f7),
        appBar: AppBar(
          centerTitle: true,
          leading: Icon(Icons.search, color: Colors.white),
          backgroundColor: Colors.black,
          bottom: TabBar(
            isScrollable: true,
            indicatorColor: Colors.white,
            // labelColor: Colors.grey[800],
            tabs: [
              Text('Latest',
                  style: TextStyle(
                    color: Colors.white,
                  )),
              Text('India',
                  style: TextStyle(
                    color: Colors.white,
                  )),
              Text('World',
                  style: TextStyle(
                    color: Colors.white,
                  )),
              Text('Business',
                  style: TextStyle(
                    color: Colors.white,
                  )),
              Text('Technology',
                  style: TextStyle(
                    color: Colors.white,
                  )),
              Text('Bookmark',
                  style: TextStyle(
                    color: Colors.white,
                  )),
            ],
          ),
          title: Text('Headlines', style: TextStyle(color: Colors.white)),
          actions: <Widget>[
            Icon(
              Icons.account_circle,
              color: Colors.white,
            )
          ],
        ),
        body: TabBarView(
          children: [
            new FutureBuilder<Map>(
                future: getStatusTokenList(),
                builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return new Text('Waiting to start');
                    case ConnectionState.waiting:
                      return Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(strokeWidth: 5.0));

                    default:
                      if (snapshot.hasError) {
                        return OfflineBuilder(connectivityBuilder: (
                          BuildContext context,
                          ConnectivityResult connectivity,
                          Widget child,
                        ) {
                          if (connectivity == ConnectivityResult.none) {
                            return Scaffold(
                              body: new Container(
                                  decoration: new BoxDecoration(
                                    image: new DecorationImage(
                                      image:
                                          new AssetImage("assets/internet.png"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  child: Center(
                                      child: Column(
                                    children: <Widget>[
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 180.0,
                                              horizontal: 0.0)),
                                      Text(
                                        "Uh oh!",
                                        style: new TextStyle(
                                          fontSize: 40.0,
                                          color: const Color(0xFF000000),
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 6.0, horizontal: 0.0)),
                                      Text(
                                        "Please check your internet connection",
                                        style: new TextStyle(
                                          fontSize: 20.0,
                                          color: const Color(0xFF000000),
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 4.0, horizontal: 0.0)),
                                      RaisedButton(
                                          color: Color(0xFFff6b6b),
                                          child: Text('Retry',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15)),
                                          onPressed: () {
                                            Navigator.pushNamed(
                                                context, Home.tag);
                                          }),
                                    ],
                                  ))),
                            );
                          } else {
                            return child;
                          }
                        }, builder: (BuildContext context) {
                          return Scaffold(
                            body: new Container(
                                decoration: new BoxDecoration(
                                  image: new DecorationImage(
                                    image:
                                        new AssetImage("assets/internet.png"),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                child: Center(
                                    child: Column(
                                  children: <Widget>[
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 150.0, horizontal: 0.0)),
                                    Text(
                                      "Uh oh!",
                                      style: new TextStyle(
                                        fontSize: 40.0,
                                        color: const Color(0xFF000000),
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 6.0, horizontal: 0.0)),
                                    Text(
                                      "Please check your internet connection",
                                      style: new TextStyle(
                                        fontSize: 20.0,
                                        color: const Color(0xFF000000),
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 4.0, horizontal: 0.0)),
                                    RaisedButton(
                                        color: Color(0xFFff6b6b),
                                        child: Text('Retry',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15)),
                                        onPressed: () {
                                          Navigator.pushNamed(
                                              context, Home.tag);
                                        }),
                                  ],
                                ))),
                          );
                        });

                        // return new Text('You have not written any feedback');
                      } else {
                        return Container(
                            child: ListView.builder(
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: snapshot.data['articles'].length,
                                itemBuilder: (context, int i) {
                                  return Padding(
                                      padding:
                                          EdgeInsets.fromLTRB(20, 30, 20, 20),
                                      child: Card(
                                          color: Colors.black12,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 10, 20, 2),
                                                  child: Text(
                                                    snapshot.data['articles'][i]
                                                        ['title'],
                                                    softWrap: true,
                                                    style: new TextStyle(
                                                      fontSize: 16.0,
                                                      color: Colors.white,
                                                      // fontWeight: FontWeight.bold,
                                                      fontFamily: 'Rubik',
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 6, 20, 2),
                                                  child: Text(
                                                    snapshot.data['articles'][i]
                                                        ['description'],
                                                    softWrap: true,
                                                    style: new TextStyle(
                                                      fontSize: 13.0,
                                                      color: Colors.white,
                                                      // fontWeight: FontWeight.bold,
                                                      fontFamily: 'Rubik',
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  )),
                                              Hero(
                                                  tag: 'rt',
                                                  child: Image.network(
                                                    snapshot.data['articles'][i]
                                                        ['urlToImage'],
                                                  )),

                                              FlatButton.icon(
                                                icon: Icon(
                                                  Icons.bookmark_border,
                                                  // color: bookmark == true
                                                  //     ? Colors.black
                                                  //     : Colors.yellow,
                                                ),
                                                label: Text('Bookmark'),
                                                onPressed: () {
                                                  setState(() {
                                                    bookmark = false;
                                                    book.add(snapshot
                                                        .data['articles'][i]);

                                                    showDialog(
                                                        context: context,
                                                        child: new AlertDialog(
                                                          title: new Text(""),
                                                          content: ListView(
                                                            shrinkWrap: true,
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons
                                                                    .check_circle,
                                                                color: Colors
                                                                    .green,
                                                                size: 35,
                                                              ),
                                                              Center(
                                                                child: Text(
                                                                  'Added to Bookmark',
                                                                  style:
                                                                      new TextStyle(
                                                                    fontSize:
                                                                        13,

                                                                    // fontWeight: FontWeight.bold,
                                                                    // fontFamily: 'Rubik',
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ));
                                                  });
                                                },
                                              )
                                              // Image(image:snapshot.data['articles'][i]['urlToImage'])
                                            ],
                                          )));
                                }));
                      }
                  }
                }),
            new FutureBuilder<Map>(
                future: getIndiaList(),
                builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return new Text('Waiting to start');
                    case ConnectionState.waiting:
                      return Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(strokeWidth: 5.0));

                    default:
                      if (snapshot.hasError) {
                        return OfflineBuilder(connectivityBuilder: (
                          BuildContext context,
                          ConnectivityResult connectivity,
                          Widget child,
                        ) {
                          if (connectivity == ConnectivityResult.none) {
                            return Scaffold(
                              body: new Container(
                                  decoration: new BoxDecoration(
                                    image: new DecorationImage(
                                      image:
                                          new AssetImage("assets/internet.png"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  child: Center(
                                      child: Column(
                                    children: <Widget>[
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 180.0,
                                              horizontal: 0.0)),
                                      Text(
                                        "Uh oh!",
                                        style: new TextStyle(
                                          fontSize: 40.0,
                                          color: const Color(0xFF000000),
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 6.0, horizontal: 0.0)),
                                      Text(
                                        "Please check your internet connection",
                                        style: new TextStyle(
                                          fontSize: 20.0,
                                          color: const Color(0xFF000000),
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 4.0, horizontal: 0.0)),
                                      RaisedButton(
                                          color: Color(0xFFff6b6b),
                                          child: Text('Retry',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15)),
                                          onPressed: () {
                                            Navigator.pushNamed(
                                                context, Home.tag);
                                          }),
                                    ],
                                  ))),
                            );
                          } else {
                            return child;
                          }
                        }, builder: (BuildContext context) {
                          return Scaffold(
                            body: new Container(
                                decoration: new BoxDecoration(
                                  image: new DecorationImage(
                                    image:
                                        new AssetImage("assets/internet.png"),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                child: Center(
                                    child: Column(
                                  children: <Widget>[
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 150.0, horizontal: 0.0)),
                                    Text(
                                      "Uh oh!",
                                      style: new TextStyle(
                                        fontSize: 40.0,
                                        color: const Color(0xFF000000),
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 6.0, horizontal: 0.0)),
                                    Text(
                                      "Please check your internet connection",
                                      style: new TextStyle(
                                        fontSize: 20.0,
                                        color: const Color(0xFF000000),
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 4.0, horizontal: 0.0)),
                                    RaisedButton(
                                        color: Color(0xFFff6b6b),
                                        child: Text('Retry',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15)),
                                        onPressed: () {
                                          Navigator.pushNamed(
                                              context, Home.tag);
                                        }),
                                  ],
                                ))),
                          );
                        });
                        // return new Text('You have not written any feedback');
                      } else {
                        return Container(
                            child: ListView.builder(
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: snapshot.data['articles'].length,
                                itemBuilder: (context, int i) {
                                  return Padding(
                                      padding:
                                          EdgeInsets.fromLTRB(20, 30, 20, 20),
                                      child: Card(
                                          color: Colors.black12,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 10, 20, 2),
                                                  child: Text(
                                                    snapshot.data['articles'][i]
                                                        ['title'],
                                                    softWrap: true,
                                                    style: new TextStyle(
                                                      fontSize: 16.0,
                                                      color: Colors.white,
                                                      // fontWeight: FontWeight.bold,
                                                      fontFamily: 'Rubik',
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 6, 20, 2),
                                                  child: Text(
                                                    snapshot.data['articles'][i]
                                                        ['description'],
                                                    softWrap: true,
                                                    style: new TextStyle(
                                                      fontSize: 13.0,
                                                      color: Colors.white,
                                                      // fontWeight: FontWeight.bold,
                                                      fontFamily: 'Rubik',
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  )),
                                              Hero(
                                                  tag: 'rt',
                                                  child: Image.network(
                                                    snapshot.data['articles'][i]
                                                        ['urlToImage'],
                                                  )),
                                              FlatButton.icon(
                                                icon: Icon(
                                                  Icons.bookmark_border,
                                                  // color: bookmark == true
                                                  //     ? Colors.black
                                                  //     : Colors.yellow,
                                                ),
                                                label: Text('Bookmark'),
                                                onPressed: () {
                                                  setState(() {
                                                    bookmark = false;
                                                    book.add(snapshot
                                                        .data['articles'][i]);

                                                    showDialog(
                                                        context: context,
                                                        child: new AlertDialog(
                                                          title: new Text(""),
                                                          content: ListView(
                                                            shrinkWrap: true,
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons
                                                                    .check_circle,
                                                                color: Colors
                                                                    .green,
                                                                size: 35,
                                                              ),
                                                              Center(
                                                                child: Text(
                                                                  'Added to Bookmark',
                                                                  style:
                                                                      new TextStyle(
                                                                    fontSize:
                                                                        13,

                                                                    // fontWeight: FontWeight.bold,
                                                                    // fontFamily: 'Rubik',
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ));
                                                  });
                                                },
                                              )

                                              // Image(image:snapshot.data['articles'][i]['urlToImage'])
                                            ],
                                          )));
                                }));
                      }
                  }
                }),
            new FutureBuilder<Map>(
                future: getWorldList(),
                builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return new Text('Waiting to start');
                    case ConnectionState.waiting:
                      return Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(strokeWidth: 5.0));

                    default:
                      if (snapshot.hasError) {
                        return OfflineBuilder(connectivityBuilder: (
                          BuildContext context,
                          ConnectivityResult connectivity,
                          Widget child,
                        ) {
                          if (connectivity == ConnectivityResult.none) {
                            return Scaffold(
                              body: new Container(
                                  decoration: new BoxDecoration(
                                    image: new DecorationImage(
                                      image:
                                          new AssetImage("assets/internet.png"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  child: Center(
                                      child: Column(
                                    children: <Widget>[
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 180.0,
                                              horizontal: 0.0)),
                                      Text(
                                        "Uh oh!",
                                        style: new TextStyle(
                                          fontSize: 40.0,
                                          color: const Color(0xFF000000),
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 6.0, horizontal: 0.0)),
                                      Text(
                                        "Please check your internet connection",
                                        style: new TextStyle(
                                          fontSize: 20.0,
                                          color: const Color(0xFF000000),
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 4.0, horizontal: 0.0)),
                                      RaisedButton(
                                          color: Color(0xFFff6b6b),
                                          child: Text('Retry',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15)),
                                          onPressed: () {
                                            Navigator.pushNamed(
                                                context, Home.tag);
                                          }),
                                    ],
                                  ))),
                            );
                          } else {
                            return child;
                          }
                        }, builder: (BuildContext context) {
                          return Scaffold(
                            body: new Container(
                                decoration: new BoxDecoration(
                                  image: new DecorationImage(
                                    image:
                                        new AssetImage("assets/internet.png"),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                child: Center(
                                    child: Column(
                                  children: <Widget>[
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 150.0, horizontal: 0.0)),
                                    Text(
                                      "Uh oh!",
                                      style: new TextStyle(
                                        fontSize: 40.0,
                                        color: const Color(0xFF000000),
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 6.0, horizontal: 0.0)),
                                    Text(
                                      "Please check your internet connection",
                                      style: new TextStyle(
                                        fontSize: 20.0,
                                        color: const Color(0xFF000000),
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 4.0, horizontal: 0.0)),
                                    RaisedButton(
                                        color: Color(0xFFff6b6b),
                                        child: Text('Retry',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15)),
                                        onPressed: () {
                                          Navigator.pushNamed(
                                              context, Home.tag);
                                        }),
                                  ],
                                ))),
                          );
                        });
                        // return new Text('You have not written any feedback');
                      } else {
                        return Container(
                            child: ListView.builder(
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: snapshot.data['articles'].length,
                                itemBuilder: (context, int i) {
                                  return Padding(
                                      padding:
                                          EdgeInsets.fromLTRB(20, 30, 20, 20),
                                      child: Card(
                                          color: Colors.black12,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 10, 20, 2),
                                                  child: Text(
                                                    snapshot.data['articles'][i]
                                                        ['title'],
                                                    softWrap: true,
                                                    style: new TextStyle(
                                                      fontSize: 16.0,
                                                      color: Colors.white,
                                                      // fontWeight: FontWeight.bold,
                                                      fontFamily: 'Rubik',
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 6, 20, 2),
                                                  child: Text(
                                                    snapshot.data['articles'][i]
                                                        ['description'],
                                                    softWrap: true,
                                                    style: new TextStyle(
                                                      fontSize: 13.0,
                                                      color: Colors.white,
                                                      // fontWeight: FontWeight.bold,
                                                      fontFamily: 'Rubik',
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  )),
                                              Hero(
                                                  tag: 'rt',
                                                  child: Image.network(
                                                    snapshot.data['articles'][i]
                                                        ['urlToImage'],
                                                  )),
                                              FlatButton.icon(
                                                icon: Icon(
                                                  Icons.bookmark_border,
                                                  //   color: bookmark == true
                                                  //       ? Colors.black
                                                  //       : Colors.yellow,
                                                ),
                                                label: Text('Bookmark'),
                                                onPressed: () {
                                                  setState(() {
                                                    bookmark = false;
                                                    book.add(snapshot
                                                        .data['articles'][i]);

                                                    showDialog(
                                                        context: context,
                                                        child: new AlertDialog(
                                                          title: new Text(""),
                                                          content: ListView(
                                                            shrinkWrap: true,
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons
                                                                    .check_circle,
                                                                color: Colors
                                                                    .green,
                                                                size: 35,
                                                              ),
                                                              Center(
                                                                child: Text(
                                                                  'Added to Bookmark',
                                                                  style:
                                                                      new TextStyle(
                                                                    fontSize:
                                                                        13,

                                                                    // fontWeight: FontWeight.bold,
                                                                    // fontFamily: 'Rubik',
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ));
                                                  });
                                                },
                                              )

                                              // Image(image:snapshot.data['articles'][i]['urlToImage'])
                                            ],
                                          )));
                                }));
                      }
                  }
                }),
            new FutureBuilder<Map>(
                future: getBusinessList(),
                builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return new Text('Waiting to start');
                    case ConnectionState.waiting:
                      return Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(strokeWidth: 5.0));

                    default:
                      if (snapshot.hasError) {
                        return OfflineBuilder(connectivityBuilder: (
                          BuildContext context,
                          ConnectivityResult connectivity,
                          Widget child,
                        ) {
                          if (connectivity == ConnectivityResult.none) {
                            return Scaffold(
                              body: new Container(
                                  decoration: new BoxDecoration(
                                    image: new DecorationImage(
                                      image:
                                          new AssetImage("assets/internet.png"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  child: Center(
                                      child: Column(
                                    children: <Widget>[
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 180.0,
                                              horizontal: 0.0)),
                                      Text(
                                        "Uh oh!",
                                        style: new TextStyle(
                                          fontSize: 40.0,
                                          color: const Color(0xFF000000),
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 6.0, horizontal: 0.0)),
                                      Text(
                                        "Please check your internet connection",
                                        style: new TextStyle(
                                          fontSize: 20.0,
                                          color: const Color(0xFF000000),
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 4.0, horizontal: 0.0)),
                                      RaisedButton(
                                          color: Color(0xFFff6b6b),
                                          child: Text('Retry',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15)),
                                          onPressed: () {
                                            Navigator.pushNamed(
                                                context, Home.tag);
                                          }),
                                    ],
                                  ))),
                            );
                          } else {
                            return child;
                          }
                        }, builder: (BuildContext context) {
                          return Scaffold(
                            body: new Container(
                                decoration: new BoxDecoration(
                                  image: new DecorationImage(
                                    image:
                                        new AssetImage("assets/internet.png"),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                child: Center(
                                    child: Column(
                                  children: <Widget>[
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 150.0, horizontal: 0.0)),
                                    Text(
                                      "Uh oh!",
                                      style: new TextStyle(
                                        fontSize: 40.0,
                                        color: const Color(0xFF000000),
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 6.0, horizontal: 0.0)),
                                    Text(
                                      "Please check your internet connection",
                                      style: new TextStyle(
                                        fontSize: 20.0,
                                        color: const Color(0xFF000000),
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 4.0, horizontal: 0.0)),
                                    RaisedButton(
                                        color: Color(0xFFff6b6b),
                                        child: Text('Retry',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15)),
                                        onPressed: () {
                                          Navigator.pushNamed(
                                              context, Home.tag);
                                        }),
                                  ],
                                ))),
                          );
                        });
                        // return new Text('You have not written any feedback');
                      } else {
                        return Container(
                            child: ListView.builder(
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: snapshot.data['articles'].length,
                                itemBuilder: (context, int i) {
                                  return Padding(
                                      padding:
                                          EdgeInsets.fromLTRB(20, 30, 20, 20),
                                      child: Card(
                                          color: Colors.black12,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 10, 20, 2),
                                                  child: Text(
                                                    snapshot.data['articles'][i]
                                                        ['title'],
                                                    softWrap: true,
                                                    style: new TextStyle(
                                                      fontSize: 16.0,
                                                      color: Colors.white,
                                                      // fontWeight: FontWeight.bold,
                                                      fontFamily: 'Rubik',
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 6, 20, 2),
                                                  child: Text(
                                                    snapshot.data['articles'][i]
                                                        ['description'],
                                                    softWrap: true,
                                                    style: new TextStyle(
                                                      fontSize: 13.0,
                                                      color: Colors.white,
                                                      // fontWeight: FontWeight.bold,
                                                      fontFamily: 'Rubik',
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  )),
                                              Hero(
                                                  tag: 'rt',
                                                  child: Image.network(
                                                    snapshot.data['articles'][i]
                                                        ['urlToImage'],
                                                  )),
                                              FlatButton.icon(
                                                icon: Icon(
                                                  Icons.bookmark_border,
                                                  // color: bookmark == true
                                                  //     ? Colors.black
                                                  //     : Colors.yellow,
                                                ),
                                                label: Text('Bookmark'),
                                                onPressed: () {
                                                  setState(() {
                                                    bookmark = false;
                                                    book.add(snapshot
                                                        .data['articles'][i]);

                                                    showDialog(
                                                        context: context,
                                                        child: new AlertDialog(
                                                          title: new Text(""),
                                                          content: ListView(
                                                            shrinkWrap: true,
                                                            children: <Widget>[
                                                              Icon(
                                                                Icons
                                                                    .check_circle,
                                                                color: Colors
                                                                    .green,
                                                                size: 35,
                                                              ),
                                                              Center(
                                                                child: Text(
                                                                  'Added to Bookmark',
                                                                  style:
                                                                      new TextStyle(
                                                                    fontSize:
                                                                        13,

                                                                    // fontWeight: FontWeight.bold,
                                                                    // fontFamily: 'Rubik',
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ));
                                                  });
                                                },
                                              )

                                              // Image(image:snapshot.data['articles'][i]['urlToImage'])
                                            ],
                                          )));
                                }));
                      }
                  }
                }),
            new FutureBuilder<Map>(
                future: getTechList(),
                builder: (BuildContext context, AsyncSnapshot<Map> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return new Text('Waiting to start');
                    case ConnectionState.waiting:
                      return Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(strokeWidth: 5.0));

                    default:
                      if (snapshot.hasError) {
                        return OfflineBuilder(connectivityBuilder: (
                          BuildContext context,
                          ConnectivityResult connectivity,
                          Widget child,
                        ) {
                          if (connectivity == ConnectivityResult.none) {
                            return Scaffold(
                              body: new Container(
                                  decoration: new BoxDecoration(
                                    image: new DecorationImage(
                                      image:
                                          new AssetImage("assets/internet.png"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  child: Center(
                                      child: Column(
                                    children: <Widget>[
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 180.0,
                                              horizontal: 0.0)),
                                      Text(
                                        "Uh oh!",
                                        style: new TextStyle(
                                          fontSize: 40.0,
                                          color: const Color(0xFF000000),
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 6.0, horizontal: 0.0)),
                                      Text(
                                        "Please check your internet connection",
                                        style: new TextStyle(
                                          fontSize: 20.0,
                                          color: const Color(0xFF000000),
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Padding(
                                          padding: new EdgeInsets.symmetric(
                                              vertical: 4.0, horizontal: 0.0)),
                                      RaisedButton(
                                          color: Color(0xFFff6b6b),
                                          child: Text('Retry',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15)),
                                          onPressed: () {
                                            Navigator.pushNamed(
                                                context, Home.tag);
                                          }),
                                    ],
                                  ))),
                            );
                          } else {
                            return child;
                          }
                        }, builder: (BuildContext context) {
                          return Scaffold(
                            body: new Container(
                                decoration: new BoxDecoration(
                                  image: new DecorationImage(
                                    image:
                                        new AssetImage("assets/internet.png"),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                child: Center(
                                    child: Column(
                                  children: <Widget>[
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 150.0, horizontal: 0.0)),
                                    Text(
                                      "Uh oh!",
                                      style: new TextStyle(
                                        fontSize: 40.0,
                                        color: const Color(0xFF000000),
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 6.0, horizontal: 0.0)),
                                    Text(
                                      "Please check your internet connection",
                                      style: new TextStyle(
                                        fontSize: 20.0,
                                        color: const Color(0xFF000000),
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    Padding(
                                        padding: new EdgeInsets.symmetric(
                                            vertical: 4.0, horizontal: 0.0)),
                                    RaisedButton(
                                        color: Color(0xFFff6b6b),
                                        child: Text('Retry',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15)),
                                        onPressed: () {
                                          Navigator.pushNamed(
                                              context, Home.tag);
                                        }),
                                  ],
                                ))),
                          );
                        });
                        // return new Text('You have not written any feedback');
                      } else {
                        return Container(
                            child: ListView.builder(
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: snapshot.data['articles'].length,
                                itemBuilder: (context, int i) {
                                  return Padding(
                                      padding:
                                          EdgeInsets.fromLTRB(20, 30, 20, 20),
                                      child: Card(
                                          color: Colors.black12,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 10, 20, 2),
                                                  child: Text(
                                                    snapshot.data['articles'][i]
                                                        ['title'],
                                                    softWrap: true,
                                                    style: new TextStyle(
                                                      fontSize: 16.0,
                                                      color: Colors.white,
                                                      // fontWeight: FontWeight.bold,
                                                      fontFamily: 'Rubik',
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 6, 20, 2),
                                                  child: Text(
                                                    snapshot.data['articles'][i]
                                                        ['description'],
                                                    softWrap: true,
                                                    style: new TextStyle(
                                                      fontSize: 13.0,
                                                      color: Colors.white,
                                                      // fontWeight: FontWeight.bold,
                                                      fontFamily: 'Rubik',
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  )),
                                              Hero(
                                                  tag: 'rt',
                                                  child: Image.network(
                                                    snapshot.data['articles'][i]
                                                        ['urlToImage'],
                                                  )),
                                              FlatButton.icon(
                                                icon: Icon(
                                                  Icons.bookmark_border,
                                                  // color: bookmark == true
                                                  //     ? Colors.black
                                                  //     : Colors.yellow,
                                                ),
                                                label: Text('Bookmark'),
                                                onPressed: () {
                                                  setState(() {
                                                    bookmark = false;
                                                    book.add(snapshot
                                                        .data['articles'][i]);
                                                  });
                                                },
                                              )
                                              // Image(image:snapshot.data['articles'][i]['urlToImage'])
                                            ],
                                          )));
                                }));
                      }
                  }
                }),
            ListView.builder(
                physics: ScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: book.length,
                itemBuilder: (context, int i) {
                  return Padding(
                      padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
                      child: Card(
                          color: Colors.black12,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.fromLTRB(20, 10, 20, 2),
                                    child: Text(
                                      book[i]['title'],
                                      softWrap: true,
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.white,
                                        // fontWeight: FontWeight.bold,
                                        fontFamily: 'Rubik',
                                      ),
                                      textAlign: TextAlign.center,
                                    )),
                                Padding(
                                    padding: EdgeInsets.fromLTRB(20, 6, 20, 2),
                                    child: Text(
                                      book[i]['description'],
                                      softWrap: true,
                                      style: new TextStyle(
                                        fontSize: 13.0,
                                        color: Colors.white,
                                        // fontWeight: FontWeight.bold,
                                        fontFamily: 'Rubik',
                                      ),
                                      textAlign: TextAlign.left,
                                    )),
                                Hero(
                                    tag: 'rt',
                                    child: Image.network(
                                      book[i]['urlToImage'],
                                    )),
                              ])));
                })
          ],
        ),
      ),
    );
  }
}
